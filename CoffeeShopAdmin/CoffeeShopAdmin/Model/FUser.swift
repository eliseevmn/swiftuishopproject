import Foundation
import Firebase

class FUser {
    let id: String
    var email: String
    var firstName: String
    var lastName: String
    var fullName: String
    var phoneNumber: String

    var fullAddress: String
    var onBoarding: Bool

    // MARK: - Init
    init(idUser: String,
         email: String,
         firstName: String,
         lastName: String,
         phoneNumber: String,
         address: String) {
        self.id = idUser
        self.email = email
        self.firstName = firstName
        self.lastName = lastName
        self.fullName = firstName + " " + lastName
        self.phoneNumber = phoneNumber
        self.fullAddress = address
        self.onBoarding = false
    }

    init(dictionary: NSDictionary) {
        self.id = dictionary[kID] as? String ?? ""
        self.email = dictionary[kEMAIL] as? String ?? ""
        self.firstName = dictionary[kFIRSTNAME] as? String ?? ""
        self.lastName = dictionary[kLASTNAME] as? String ?? ""
        self.fullName = firstName + " " + lastName
        self.fullAddress = dictionary[kFULLADRESS] as? String ?? ""
        self.phoneNumber = dictionary[kPHONENUMBER] as? String ?? ""
        self.onBoarding = dictionary[kONBOARD] as? Bool ?? false
    }

    // MARK: - FUser functions
    class func currentId() -> String {
        return Auth.auth().currentUser!.uid
    }

    class func currentUser() -> FUser? {
        if Auth.auth().currentUser != nil {
            if let dictionary = userDefaults.object(forKey: kCURRENTUSER) {
                return FUser(dictionary: dictionary as? NSDictionary ?? NSDictionary())
            }
        }
        return nil
    }

    class func loginUserWith(email: String,
                             password: String,
                             completion: @escaping (_ error: Error?, _ isEmailVerified: Bool) -> Void) {
        Auth.auth().signIn(withEmail: email, password: password) { (authDataResult, error) in
            guard let authDataResult = authDataResult else {
                completion(error, false)
                return
            }

            if error == nil {
                if authDataResult.user.isEmailVerified {
                    downloadUserFromFirestore(userId: authDataResult.user.uid, email: email) { (error) in
                        completion(error, true)
                    }
                } else {
                    print("Email is not verified")
                    completion(error, false)
                }
            } else {
                completion(error, false)
            }
        }
    }

    class func registerUserWith(email: String, password: String, completion: @escaping(_ error: Error?) -> Void) {
        Auth.auth().createUser(withEmail: email, password: password) { (authDataResult, error) in
            guard let authDataResult = authDataResult else {
                return
            }

            if error == nil {
                authDataResult.user.sendEmailVerification { (error) in
                    print(error?.localizedDescription ?? "Auth email verification error")
                }
            }
        }
    }

    class func resetPassword(email: String, completion: @escaping(_ error: Error?) -> Void) {
        Auth.auth().sendPasswordReset(withEmail: email) { (error) in
            completion(error)
        }
    }

    class func logOutCurrentUser(completion: @escaping(_ error: Error?) -> Void) {
        do {
            try Auth.auth().signOut()
            userDefaults.removeObject(forKey: kCURRENTUSER)
            userDefaults.synchronize()
            completion(nil)
        } catch let error {
            completion(error)
        }
    }
}

// MARK: - Helpers FUser firebase functions
func downloadUserFromFirestore(userId: String, email: String, completion: @escaping(_ error: Error?) -> Void) {

    firebaseReference(.user).document(userId).getDocument { (snapshot, error) in
        guard let snapshot = snapshot else {
            return
        }

        if snapshot.exists {
            saveUserLocally(userDictionary: snapshot.data()! as NSDictionary)
        } else {
            let user = FUser(idUser: userId, email: email, firstName: "", lastName: "", phoneNumber: "", address: "")
            saveUserLocally(userDictionary: userDictionaryFrom(user: user) as NSDictionary)
            saveUserToFirestore(fUser: user)
        }

        completion(error)
    }
}

func saveUserToFirestore(fUser: FUser) {
    firebaseReference(.user).document(fUser.id).setData(userDictionaryFrom(user: fUser)) { (error) in
        if error != nil {
            print("Error creating ofuser object: ", error!.localizedDescription)
        }
    }
}

func saveUserLocally(userDictionary: NSDictionary) {
    userDefaults.set(userDictionary, forKey: kCURRENTUSER)
    userDefaults.synchronize()
}

func userDictionaryFrom(user: FUser) -> [String: Any] {
    return NSDictionary(objects: [
        user.id,
        user.email,
        user.firstName,
        user.lastName,
        user.fullName,
        user.fullAddress,
        user.onBoarding,
        user.phoneNumber
    ], forKeys: [
        kID as NSCopying,
        kEMAIL as NSCopying,
        kFIRSTNAME as NSCopying,
        kLASTNAME as NSCopying,
        kFULLNAME as NSCopying,
        kFULLADRESS as NSCopying,
        kONBOARD as NSCopying,
        kPHONENUMBER as NSCopying
    ]) as? [String: Any] ?? [:]
}

func updateCurrentUser(withValues: [String: Any], completion: @escaping(_ error: Error?) -> Void) {

    if let dictionary = userDefaults.object(forKey: kCURRENTUSER) {
        let userObject = (dictionary as? NSDictionary)?.mutableCopy() as? NSMutableDictionary ?? NSMutableDictionary()
        userObject.setValuesForKeys(withValues)

        firebaseReference(.user).document(FUser.currentId()).updateData(withValues) { (error) in
            completion(error)

            if error == nil {
                saveUserLocally(userDictionary: userObject)
            }
        }
    }
}
