import SwiftUI

struct ContentView: View {

    // MARK: - Properties
    @ObservedObject var orderListener = OrderListener()

    // MARK: - Body
    var body: some View {
        NavigationView {

            List {
                Section(header: Text("Active orders")) {

                    ForEach(self.orderListener.activeOrders ?? []) { order in

                        NavigationLink(destination: OrderDetailView(order: order)) {
                            HStack {
                                Text(order.customerName).lineLimit(0)
                                Spacer()
                                Text("$ \(order.amount.clean)")
                            } // End of HStack

                        } // End of Navigation link

                    } // End of ForEach

                } // End of Section 1

                Section(header: Text("Completed orders")) {
                    ForEach(self.orderListener.completedOrdes ?? []) { order in

                        NavigationLink(destination: OrderDetailView(order: order)) {
                              HStack {
                                  Text(order.customerName).lineLimit(0)
                                  Spacer()
                                  Text("$ \(order.amount.clean)")
                              } // End of HStack

                          } // End of Navigation link

                      } // End of ForEach

                } // End of Section 2

            } // End of List
            .navigationBarTitle("Orders")
        } // End of NavigationView
    }
}

// MARK: - ContentView_Previews
struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
