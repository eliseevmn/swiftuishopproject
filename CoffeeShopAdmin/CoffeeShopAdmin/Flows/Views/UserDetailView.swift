import SwiftUI

struct UserDetailView: View {

    // MARK: - Properties
    var order: Order
    @State var user: FUser?

    // MARK: - Body
    var body: some View {
        List {
            Section {
                Text(user?.fullName ?? "")
                Text(user?.email ?? "")
                Text(user?.phoneNumber ?? "")
                Text(user?.fullAddress ?? "")
            } // End Of Section

        } // End of List
            .listStyle(GroupedListStyle())
            .navigationBarTitle("User Pofile")
            .onAppear {
                self.getUser()
        }
    } // End of Body

    // MARK: - Get User function
    private func getUser() {
        downloadUser(userId: order.customerId) { (fUser) in
            self.user = fUser
        }
    }
}

// MARK: - UserDetail_Previews
struct UserDetail_Previews: PreviewProvider {
    static var previews: some View {
        UserDetailView(order: Order())
    }
}
