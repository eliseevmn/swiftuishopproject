import SwiftUI

struct OrderDetailView: View {

    // MARK: - Order
    var order: Order

    // MARK: - Body
    var body: some View {
        VStack {

            List {
                Section(header: Text("Customer")) {
                    NavigationLink(destination: UserDetailView(order: order)) {
                        Text(order.customerName)
                            .font(.headline)
                    } // End of Navigation Link
                } // Enf of Section 1

                Section(header: Text("Order items")) {
                    ForEach(order.orderItems) { drink in
                        HStack {
                            Text(drink.name)
                            Spacer()
                            Text("$ \(drink.price.clean)")
                        } // End of HStack

                    } // End of ForEach

                } // End of Section 2

            } // End of list

        } // End of VStack
            .navigationBarTitle("Order", displayMode: .inline)
            .navigationBarItems(trailing:
                Button(action: {
                    print("complete order")
                    self.markAsCompleted()
                }, label: {
                    if !order.isCompleted {
                        Text("Complete order")
                    } else {
                        Text("Not Complete order")
                    }
                })
        )
    } // End of body

    // MARK: - Helpers functions
    private func markAsCompleted() {
        if !order.isCompleted {
            order.isCompleted = true
            order.saveOrderToFirestore()
        } else {
            order.isCompleted = false
            order.saveOrderToFirestore()
        }
    }
}

// MARK: - OrderDetailView_Previews
struct OrderDetailView_Previews: PreviewProvider {
    static var previews: some View {
        OrderDetailView(order: Order())
    }
}
