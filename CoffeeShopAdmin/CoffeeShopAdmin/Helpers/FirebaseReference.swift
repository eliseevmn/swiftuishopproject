import Foundation
import FirebaseFirestore

enum FCollectionReference: String {
    case user = "User"
    case menu = "Menu"
    case order = "Order"
    case basket = "Basket"
}

func firebaseReference(_ collectionReference: FCollectionReference) -> CollectionReference {
    return Firestore.firestore().collection(collectionReference.rawValue)
}
