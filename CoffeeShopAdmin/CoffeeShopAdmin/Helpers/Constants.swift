import Foundation

public let userDefaults = UserDefaults.standard

//Drink
public let kID = "id"
public let kNAME = "name"
public let kPRICE = "price"
public let kDESCRIPTION = "description"
public let kCATEGORY = "category"
public let kIMAGENAME = "imageName"

//Order
public let kDRINKIDS = "categoryId"
public let kOWNERID = "description"
public let kCUSTOMERID = "price"
public let kAMOUNT = "amount"
public let kCUSTOMERNAME = "customerName"
public let kISCOMPLETED = "isCompleted"

//FUser
public let kEMAIL = "email"
public let kFIRSTNAME = "firstName"
public let kLASTNAME = "lastName"
public let kFULLNAME = "fullName"
public let kCURRENTUSER = "currentUser"
public let kFULLADRESS = "fullAdress"
public let kPHONENUMBER = "phoneNumber"
public let kONBOARD = "onBoard"
