import Foundation
import Firebase

// MARK: - Firebase user functions
func downloadUser(userId: String, completion: @escaping(_ user: FUser?) -> Void) {
    firebaseReference(.user).whereField(kID, isEqualTo: userId).getDocuments { (snapshot, _) in

        guard let snapshot = snapshot else { return }

        if !snapshot.isEmpty {
            let userData = snapshot.documents.first!.data()
            completion(FUser(dictionary: userData as NSDictionary))
        } else {
            completion(nil)
        }
    }
}
