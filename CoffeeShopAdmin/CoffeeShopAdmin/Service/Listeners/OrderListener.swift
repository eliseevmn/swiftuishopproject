import Foundation
import Firebase

class OrderListener: ObservableObject {

    // MARK: - Properties
    @Published var activeOrders: [Order]!
    @Published var completedOrdes: [Order]!

    // MARK: - Init
    init() {
        downloadOrder()
    }

    // MARK: - Order functions
    func downloadOrder() {
        firebaseReference(.order).addSnapshotListener { (snapshot, error) in
            if let error = error {
                print(error)
                return
            }

            guard let snapshot = snapshot else {
                return
            }

            if !snapshot.isEmpty {

                self.activeOrders = []
                self.completedOrdes = []

                for order in snapshot.documents {

                    let orderData = order.data()

                    getDrinksFromFirestore(withIds: orderData[kDRINKIDS] as? [String] ?? []) { (allDrinks) in

                        let order = Order()
                        order.customerId = orderData[kCUSTOMERID] as? String
                        order.id = orderData[kID] as? String
                        order.orderItems = allDrinks
                        order.amount = orderData[kAMOUNT] as? Double
                        order.customerName = orderData[kCUSTOMERNAME] as? String ?? ""
                        order.isCompleted = orderData[kISCOMPLETED] as? Bool ?? false

                        if order.isCompleted {
                            self.completedOrdes.append(order)
                        } else {
                            self.activeOrders.append(order)
                        }
                    }
                }
            }
        }
    } // End of download function
}

// MARK: - Firebase order functions
func getDrinksFromFirestore(withIds: [String], completion: @escaping(_ drinkArray: [Drink]) -> Void) {
    var count = 0
    var drinkArray: [Drink] = []

    if withIds.count == 0 {
        completion(drinkArray)
        return
    }

    for drinkId in withIds {
        firebaseReference(.menu).whereField(kID, isEqualTo: drinkId).getDocuments { (snapshot, _) in

            guard let snapshot = snapshot else {
                return
            }

            if !snapshot.isEmpty {
                let drinkData = snapshot.documents.first!
                drinkArray.append(Drink(id: drinkData[kID] as? String ?? UUID().uuidString,
                                       name: drinkData[kNAME] as? String ?? "unknown",
                                       imageName: drinkData[kIMAGENAME] as? String ?? "unknown",
                                       category: Category(rawValue: drinkData[kCATEGORY] as? String ?? "Cold") ?? .cold,
                                       description: drinkData[kDESCRIPTION] as? String ?? "Description is missing",
                                       price: drinkData[kPRICE] as? Double ?? 0.0))
                count += 1
            } else {
                print("Have no drink")
                completion(drinkArray)
            }

            if count == withIds.count {
                completion(drinkArray)
            }
        }
    }
}
