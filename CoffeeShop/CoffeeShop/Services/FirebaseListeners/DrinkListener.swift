import Foundation
import Firebase
import SwiftUI

class DrinkListener: ObservableObject {

    // MARK: - Properties
    @Published var drinks: [Drink] = []

    // Init
    init() {
        downloadDrinks()
    }

    // MARK: - Download functions
    func downloadDrinks() {
        firebaseReference(.menu).getDocuments { (snapshot, error) in
            if let error = error {
                print(error)
                return
            }

            guard let snapshot = snapshot else {
                return
            }

            if !snapshot.isEmpty {
                self.drinks = DrinkListener.drinkFromDictionary(snapshot: snapshot)
            }
        }
    }

    static func drinkFromDictionary(snapshot: QuerySnapshot) -> [Drink] {
        var allDrinks: [Drink] = []
        for snapshot in snapshot.documents {
            let drinkData = snapshot.data()
            allDrinks.append(Drink(id: drinkData[kID] as? String ?? UUID().uuidString,
                                   name: drinkData[kNAME] as? String ?? "unknown",
                                   imageName: drinkData[kIMAGENAME] as? String ?? "unknown",
                                   category: Category(rawValue: drinkData[kCATEGORY] as? String ?? "Cold") ?? .cold,
                                   description: drinkData[kDESCRIPTION] as? String ?? "Description is missing",
                                   price: drinkData[kPRICE] as? Double ?? 0.0))
        }
        return allDrinks
    }
}
