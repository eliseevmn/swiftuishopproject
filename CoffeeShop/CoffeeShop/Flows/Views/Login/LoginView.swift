import SwiftUI

struct LoginView: View {

    // MARK: - Properties
    @State var showingSignUp = false
    @State var showingFinishReg = false

    @Environment(\.presentationMode) var presentationMode

    @State var email = ""
    @State var password = ""
    @State var repeatPassword = ""

    // MARK: - Body
    var body: some View {
        VStack {
            Text("Sign In")
                .fontWeight(.heavy)
                .font(.largeTitle)
                .padding([.bottom, .top], 20)

            VStack(alignment: .leading) {

                VStack(alignment: .leading) {
                    Text("Email")
                        .font(.headline)
                        .fontWeight(.light)
                        .foregroundColor(Color.init(.label))
                        .opacity(0.75)

                    TextField("Enter your email", text: $email)
                    Divider()

                    Text("Password")
                        .font(.headline)
                        .fontWeight(.light)
                        .foregroundColor(Color.init(.label))
                        .opacity(0.75)

                    SecureField("Enter your password", text: $password)
                    Divider()

                    if showingSignUp {
                        Text("Repeat Password")
                            .font(.headline)
                            .fontWeight(.light)
                            .foregroundColor(Color.init(.label))
                            .opacity(0.75)

                        SecureField("Repeat password", text: $repeatPassword)
                        Divider()
                    }
                } // End of VStack
                    .padding(.bottom, 15)
                    .animation(.easeOut(duration: 0.5))

                HStack {
                    Spacer()
                    Button(action: {
                        self.resetPassword()
                    }, label: {
                        Text("Forgot Password?")
                            .foregroundColor(Color.gray.opacity(0.5))
                    })
                } // End of HStack
            } // End of VStack
                .padding(.horizontal, 15)

            Button(action: {
                self.showingSignUp ? self.signUpUser() : self.loginUser()
            }, label: {
                Text(showingSignUp ? "Sign Up" : "Sign In")
                    .foregroundColor(.white)
                    .frame(width: UIScreen.main.bounds.width - 120)
                    .padding()
            }) // End of Button
                .background(Color.blue)
                .clipShape(Capsule())
                .padding(.top, 50)

            SignUpView(showingSignUp: $showingSignUp)

        } // End of VStack
            .sheet(isPresented: $showingFinishReg) {
                FinishRegistrationView()
        }
    } // End Of Body

    // MARK: - Registration func
    private func signUpUser() {
        if email != "" && password != "" {
            if password == repeatPassword {
                FUser.registerUserWith(email: email, password: password) { (error) in
                    if error != nil {
                        print("Error registering user: ", error!.localizedDescription)
                        return
                    }
                    print("user has been created")
                    // go back to the app
                    // check if user onboarding
                }
            } else {
                print("password don/t match")
            }

        } else {
            print("Email and password must be set")
        }
    }

    private func resetPassword() {
        if email != "" {
            FUser.resetPassword(email: email) { (error) in
                if error != nil {
                    print(error?.localizedDescription ?? "Error sending reset password")
                }
            }
        } else {
            // notify the suer
            print("Email is emty")
        }
    }

    // MARK: - Helpers func
    private func loginUser() {
        if email != "" && password != "" {
            FUser.loginUserWith(email: email, password: password) { (error, _) in

                if error != nil {
                    print("error loging in the user: ", error!.localizedDescription)
                    return
                }

                if FUser.currentUser() != nil && FUser.currentUser()!.onBoarding {
                    self.presentationMode.wrappedValue.dismiss()
                } else {
                    self.showingFinishReg.toggle()
                }
            }
        }
    }
}

// MARK: - Login_Previews
struct Login_Previews: PreviewProvider {
    static var previews: some View {
        LoginView()
    }
}
