import SwiftUI

struct SignUpView: View {

    // MARK: - Properties
    @Binding var showingSignUp: Bool

    // MARK: - Body
    var body: some View {

        VStack {
            Spacer()

            HStack(spacing: 8) {
                Text("Don't have an Account?")
                    .foregroundColor(Color.gray.opacity(0.5))

                Button(action: {
                    self.showingSignUp.toggle()
                }, label: {
                    Text("Sign Up")
                })
                    .foregroundColor(.blue)
            } // End os HStack
                .foregroundColor(.blue)

        } // End of VStack
    } // End of Body
}
