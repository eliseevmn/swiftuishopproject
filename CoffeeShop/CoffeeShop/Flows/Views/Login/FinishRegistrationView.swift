import SwiftUI

struct FinishRegistrationView: View {

    // MARK: - Properties
    @State var name = ""
    @State var surname = ""
    @State var telephone = ""
    @State var address = ""

    @Environment(\.presentationMode) var presentationMode

    // MARK: - Body
    var body: some View {

        Form {
            Section {
                Text("Finish Registration")
                    .fontWeight(.heavy)
                    .font(.largeTitle)
                    .padding([.top, .bottom], 20)
                TextField("Name", text: $name)
                TextField("Surname", text: $surname)
                TextField("Telephone", text: $telephone)
                TextField("Address", text: $address)

            } // End of Section 1

            Section {
                Button(action: {
                    self.finishRegistration()
                }, label: {
                    Text("Finish registration")
                })
            }.disabled(!self.fieldsCompleted()) // End of Section 2

        } // End of form
    } // End of Body

    // MARK: - Finish registration
    private func finishRegistration() {
        let fullname = name + " " + surname
        updateCurrentUser(withValues: [
            kFIRSTNAME: name,
            kLASTNAME: surname,
            kFULLNAME: fullname,
            kFULLADRESS: address,
            kPHONENUMBER: telephone,
            kONBOARD: true
        ]) { (error) in
            if error != nil {
                print("Error updating user: ", error!.localizedDescription)
                return
            }
            self.presentationMode.wrappedValue.dismiss()
        }
    }

    // MARK: - Helpers
    private func fieldsCompleted() -> Bool {
        return self.name != "" &&
            self.surname != "" &&
            self.telephone != "" &&
            self.address != ""
    }
}

struct FinishRegistrationView_Previews: PreviewProvider {
    static var previews: some View {
        FinishRegistrationView()
    }
}
