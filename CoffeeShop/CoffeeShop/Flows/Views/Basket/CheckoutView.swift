import SwiftUI

struct CheckoutView: View {

    // MARK: - Properties
    @ObservedObject var basketListener = BasketListener()
    static let paymentTypes = ["Cash", "Credit card"]
    static let tipAmounts = [10, 15, 20, 0]

    @State private var paymentType = 0
    @State private var tipAmount = 1
    @State private var showingPaymentAlert = false

    var totalPrice: Double {
        if basketListener.orderBasket == nil {
            return 0.0
        }
        let total = basketListener.orderBasket.total
        let tipValue = total/100 * Double(Self.tipAmounts[tipAmount])
        return total + tipValue
    }

    // MARK: - Body
    var body: some View {
        Form {
            Section {
                Picker(selection: $paymentType, label: Text("How do you want to pay?")) {
                    ForEach(0..<Self.paymentTypes.count) {
                        Text(Self.paymentTypes[$0])
                    }
                }
            } // End of Section 1

            Section(header: Text("Add a tip")) {
                Picker(selection: $tipAmount, label: Text("Percentage:")) {
                    ForEach(0..<Self.tipAmounts.count) {
                        Text("\(Self.tipAmounts[$0]) %")
                    }
                }
                .pickerStyle(SegmentedPickerStyle())
            } // End of Section 2

            Section(header: Text("Total: $  \(totalPrice, specifier: "%.2f")").font(.largeTitle)) {
                Button(action: {
                    self.showingPaymentAlert.toggle()
                    self.createrOrder()
                    self.emptyBasket()
                }, label: {
                    Text("Confirm order")
                })
            }.disabled(self.basketListener.orderBasket?.items.isEmpty ?? true)
            // End of Section 3
        } // End of Form
            .navigationBarTitle(Text("Payment"), displayMode: .inline)
            .alert(isPresented: $showingPaymentAlert) {
                Alert(title: Text("Order confirm"), message: Text("Thank You!"), dismissButton: .default(Text("OK")))
        }
    }

    // MARK: - Order funcstions
    private func createrOrder() {
        let order = Order()
        order.amount = totalPrice
        order.id = UUID().uuidString
        order.customerId = FUser.currentId()
        order.orderItems = self.basketListener.orderBasket.items
        order.saveOrderToFirestore()
    }

    private func emptyBasket() {
        self.basketListener.orderBasket.emptyBasket()
    }
}

// MARK: - CheckoutView_Previews
struct CheckoutView_Previews: PreviewProvider {
    static var previews: some View {
        CheckoutView()
    }
}
