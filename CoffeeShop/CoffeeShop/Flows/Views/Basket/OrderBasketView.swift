import SwiftUI

struct OrderBasketView: View {

    // MARK: - Properties
    @ObservedObject var basketListener = BasketListener()

    // MARK: - Body
    var body: some View {
        NavigationView {
            List {
                Section {
                    ForEach(self.basketListener.orderBasket?.items ?? []) { drink in
                        HStack {
                            Text(drink.name)
                            Spacer()
                            Text("$ \(drink.price.clean)")
                        } // End of HStack
                    } // End of ForEach
                        .onDelete { (indexSet) in
                            print("Delete at \(indexSet)")
                            self.deleteItems(at: indexSet)
                    }
                } // End of Section 1

                Section {
                    NavigationLink(destination: CheckoutView()) {
                        Text("Place Order")
                    }
                } // End of Section 2
                    .disabled(self.basketListener.orderBasket?.items.isEmpty ?? true)
            } // End of List
                .navigationBarTitle("Order")
                .listStyle(GroupedListStyle())
        } // End of NavigationView
    }

    // MARK: - Helpers functions
    func deleteItems(at offsets: IndexSet) {
        self.basketListener.orderBasket.items.remove(at: offsets.first!)
        self.basketListener.orderBasket.saveBasketToFirestore()
    }
}

// MARK: - OrderBasketView_Previews
struct OrderBasketView_Previews: PreviewProvider {
    static var previews: some View {
        OrderBasketView()
    }
}
