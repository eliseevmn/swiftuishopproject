import SwiftUI

struct DrinkDetail: View {

    // MARK: - Properties
    @State private var showingAlert = false
    @State private var showingLogin = false

    var drink: Drink

    // MARK: - Body
    var body: some View {
        ScrollView(.vertical, showsIndicators: false) {
            ZStack(alignment: .bottom) {
                Image(drink.imageName)
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                Rectangle()
                    .frame(height: 80)
                    .foregroundColor(.black)
                    .opacity(0.35)
                    .blur(radius: 10)
                HStack {
                    VStack(alignment: .leading, spacing: 8) {
                        Text(drink.name)
                            .foregroundColor(.white)
                            .font(.largeTitle)
                    } // End of VStack
                        .padding(.leading)
                        .padding(.bottom)

                    Spacer()
                } // End of HStack
            } // End of ZStack
                .listRowInsets(EdgeInsets())

            Text(drink.description)
                .foregroundColor(.primary)
                .font(.body)
                .lineLimit(5)
                .padding()

            HStack {
                Spacer()

                OrderButton(showAlert: $showingAlert, showLogin: $showingLogin, drink: self.drink)
                Spacer()
            }
            .padding(.top, 50)

        } // End of ScrollView
            .edgesIgnoringSafeArea(.top)
            .navigationBarHidden(true)
            .alert(isPresented: $showingAlert) {
                Alert(title: Text("Added to Basket!"), dismissButton: .default(Text("OK")))
        }
    }
}

// MARK: - DrinkDetail_Previews
struct DrinkDetail_Previews: PreviewProvider {
    static var previews: some View {
        DrinkDetail(drink: drinkData[0])
    }
}
