import SwiftUI

struct OrderButton: View {

    // MARK: - Properties
    @ObservedObject var basketListener = BasketListener()
    @Binding var showAlert: Bool
    @Binding var showLogin: Bool
    var drink: Drink

    // MARK: - Body
    var body: some View {
        Button(action: {
            if FUser.currentUser() != nil && FUser.currentUser()!.onBoarding {
                self.showAlert.toggle()
                self.addItemToBasket()
            } else {
                self.showLogin.toggle()
            }
        }, label: {
            Text("Add to Basket")
        })
        .frame(width: 200, height: 50)
        .foregroundColor(.white)
        .font(.headline)
        .background(Color.blue)
        .cornerRadius(10)
        .sheet(isPresented: self.$showLogin) {
            if FUser.currentUser() != nil {
                FinishRegistrationView()
            } else {
                LoginView()
            }
        }
    }

    // MARK: - Basket functions
    private func addItemToBasket() {
        var orderBasket: OrderBasket!

        if self.basketListener.orderBasket != nil {
            orderBasket = self.basketListener.orderBasket
        } else {
            orderBasket = OrderBasket()
            orderBasket.ownerId = FUser.currentId()
            orderBasket.id = UUID().uuidString
        }
        orderBasket.add(item: self.drink)
        orderBasket.saveBasketToFirestore()
    }
}
