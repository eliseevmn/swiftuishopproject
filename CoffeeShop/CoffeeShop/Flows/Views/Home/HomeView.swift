import SwiftUI

struct HomeView: View {

    // MARK: - Propertes
    @ObservedObject var drinkListener = DrinkListener()
    @State private var showingBasket = false

    var categories: [String: [Drink]] {
        .init(
            grouping: drinkListener.drinks,
            by: { $0.category.rawValue }
        )
    }

    // MARK: - Body
    var body: some View {
        NavigationView {

            List(categories.keys.sorted(), id: \String.self) { key in
                DrinkRow(categoryName: "\(key) Drink".uppercased(), drinks: self.categories[key]!)
                    .frame(height: 320)
                    .padding(.top)
                    .padding(.bottom)
            } // List

            .navigationBarTitle("CoFFeeSHop")
            .navigationBarItems(leading:
                Button(action: {
                    FUser.logOutCurrentUser { (error) in
                        print("Error logout user, ", error?.localizedDescription ?? "")
                    }
                }, label: {
                    Text("Log Out")
                }), trailing:
                Button(action: {
                    self.showingBasket.toggle()
                    print("Basket Button")
                }, label: {
                    Image("basket")
                })
                    .sheet(isPresented: $showingBasket) {
                        if FUser.currentUser() != nil && FUser.currentUser()!.onBoarding {
                            OrderBasketView()
                        } else if FUser.currentUser() != nil {
                            FinishRegistrationView()
                        } else {
                            LoginView()
                        }
                }
            )
        } // NavigationView
    } // Body
}

// MARK: - ContentView_Previews
struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}
