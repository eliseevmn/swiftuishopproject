import Foundation
import Firebase

class OrderBasket: Identifiable {
    var id: String!
    var ownerId: String!
    var items: [Drink] = []

    var total: Double {
        if items.count > 0 {
            return items.reduce(0) { $0 + $1.price }
        } else {
            return 0.0
        }
    }

    // MARK: - Basket functions
    func add(item: Drink) {
        items.append(item)
    }

    func remove(item: Drink) {
        if let index = items.firstIndex(of: item) {
            items.remove(at: index)
        }
    }

    func emptyBasket() {
        self.items = []
        // save to firebase
        saveBasketToFirestore()
    }

    func saveBasketToFirestore() {
        firebaseReference(.basket).document(self.id).setData(basketDictionaryFrom(basket: self)) { (error) in
            if let error = error {
                print(error)
                return
            }
        }
    }
}

// MARK: - Helpers basket firebase functions
func basketDictionaryFrom(basket: OrderBasket) -> [String: Any] {
    var allDrinkIds: [String] = []

    for drink in basket.items {
        allDrinkIds.append(drink.id)
    }

    return NSDictionary(objects: [
        basket.id ?? "",
        basket.ownerId ?? "",
        allDrinkIds
    ], forKeys: [
        kID as NSCopying,
        kOWNERID as NSCopying,
        kDRINKIDS as NSCopying
    ]) as? [String: Any] ?? [:]
}
