import Foundation
import SwiftUI

class Order: Identifiable {
    var id: String!
    var customerId: String!
    var orderItems: [Drink] = []
    var amount: Double!
    var customerName: String!
    var isCompleted: Bool!

     // MARK: - Basket functions
    func saveOrderToFirestore() {
        firebaseReference(.order).document(self.id).setData(orderDictionaryFfrom(order: self)) { (error) in
            if let error = error {
                print(error)
                return
            }
        }
    }
}

// MARK: - Helpers order firebase functions
func orderDictionaryFfrom(order: Order) -> [String: Any] {
    var allDrinkIds: [String] = []

    for drink in order.orderItems {
        allDrinkIds.append(drink.id)
    }

    return NSDictionary(objects: [
        order.id ?? "",
        order.customerId ?? "",
        allDrinkIds,
        order.amount ?? 0.0,
        order.customerName ?? "",
        order.isCompleted ?? ""
    ], forKeys: [
        kID as NSCopying,
        kCUSTOMERID as NSCopying,
        kDRINKIDS as NSCopying,
        kAMOUNT as NSCopying,
        kCUSTOMERNAME as NSCopying,
        kISCOMPLETED as NSCopying
    ]) as? [String: Any] ?? [:]
}
