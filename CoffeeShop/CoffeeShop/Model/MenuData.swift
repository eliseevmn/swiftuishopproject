import Foundation

// MARK: - Test data
let drinkData = [
    //HOT
    Drink(id: UUID().uuidString,
          name: "Espresso",
          imageName: "espresso",
          category: Category.hot,
          description: "Espresso is the purest distillation of the coffee bean.",
          price: 2.50),

    Drink(id: UUID().uuidString,
          name: "Americano",
          imageName: "americano",
          category: Category.hot,
          description: "An Americano Coffee is an Espresso-based coffee drink with no special additions.",
          price: 2.00),

    Drink(id: UUID().uuidString,
          name: "Cappuccino",
          imageName: "cappuccino",
          category: Category.hot,
          description: "Outside of Italy, cappuccino is a coffee drink that today is typically composed",
          price: 2.50),

    Drink(id: UUID().uuidString,
          name: "Latte",
          imageName: "latte",
          category: Category.hot,
          description: "A typical latte is made with six to eight ounces of steamed milk and one shot of espresso.",
          price: 2.50),

    //FILTER
    Drink(id: UUID().uuidString,
          name: "Filter Classic",
          imageName: "filter coffee",
          category: Category.filter,
          description: "Filter coffee brewing involves pouring hot water over coffee grounds.",
          price: 2.00),

    Drink(id: UUID().uuidString,
          name: "Filter Decaf",
          imageName: "decaf",
          category: Category.filter,
          description: "Filter coffee brewing involves pouring hot water over coffee grounds.",
          price: 2.00),

    Drink(id: UUID().uuidString,
          name: "Cold Brew",
          imageName: "cold brew",
          category: Category.filter,
          description: "Cold brew is really as simple as mixing ground coffee with cool water and",
          price: 2.50),

    Drink(id: UUID().uuidString,
          name: "Cold Brew Latte",
          imageName: "brew latte",
          category: Category.filter,
          description: "To make a weaker brew, add 2 parts cold brew coffee to 1 part hot water.",
          price: 2.00),

    //COLD
    Drink(id: UUID().uuidString,
          name: "Frappe",
          imageName: "frappe",
          category: Category.cold,
          description: "Frappé coffee is a Greek iced coffee drink made from instant coffee, water and sugar.",
          price: 5.00),

    Drink(id: UUID().uuidString,
          name: "Freddo Espresso",
          imageName: "freddo espresso",
          category: Category.cold,
          description: "A Freddo Espresso is basically 1 shot of espresso poured hot into a metal canister.",
          price: 5.00),

    Drink(id: UUID().uuidString,
          name: "Freddo Cappucciono",
          imageName: "freddo cappuccino",
          category: Category.cold,
          description: "The Freddo Cappuccino is pretty much a Freddo Espresso with a lovely",
          price: 4.00),

    Drink(id: UUID().uuidString,
          name: "Americano",
          imageName: "ice americano",
          category: Category.cold,
          description: "An Americano Coffee is an Espresso-based coffee drink with no special additions.",
          price: 2.00),

    Drink(id: UUID().uuidString,
          name: "Iced Latte",
          imageName: "iced latte",
          category: Category.cold,
          description: "The latte is one of the most iconic espresso drinks, favored for its frothy foam topping.",
          price: 2.50)
]
